var cfg;
var panic = false;
var prompt_cb = null;
var write_method = 'print';
var token = null;
const html = {
    encode: function(text){
        return $('<textarea></textarea>').text(text).html().replace(/&not;/g, '¬');
    },
    decode: function(text){
        return $('<textarea></textarea>').html(text).text();
    }
}
function guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

var pre = $('pre');
function scroll_to_bottom() {
    $('html, body').scrollTop($(document).height()-$(window).height());
}

var mask = false;
var echo = false;

function print(message){
    return new Promise((resolve, reject)=>{
        var html = pre.html();
        pre.html(`${html}${intColor(message)}\n`);
        scroll_to_bottom();
        resolve();
    });
}

var delays = new Map();

function animateLoop(){
    var darr = Array.from(delays);
    darr.forEach((el, i)=>{
        if(i == 0){
            var s = el[1];
            if(!s.lock){
                s.lock = true;
                if(s.val == s.raw.length){
                    $(`#${s.uuid}`).html(intColor(s.raw));
                    scroll_to_bottom();
                    s.resolve();
                    delays.delete(s.uuid);
                } else if(s.time <= Date.now()){
                    s.time = Date.now()+s.delay;
                    s.val += 1;
                    $(`#${s.uuid}`).html(intColor(s.raw.substring(0, s.val)).replace(/¬/g, ''));
                    scroll_to_bottom();
                    s.lock = false;
                } else {
                    s.lock = false;
                }
            }
        }
    });
}

setInterval(animateLoop, 5);

function print_delay(text, delay = 2){
    var html = pre.html();
    var uid = guidGenerator();
    var promise = new Promise((resolve, reject)=>{
        pre.html(`${html}<span id="${uid}"></span>\n`);
        delays.set(uid, {resolve: resolve, lock: false, raw: text, uuid: uid, val: 0, time: Date.now()+delay, delay: delay});
    });
    scroll_to_bottom();
    return promise;
}

function write(text){
    if(write_method == 'print'){
        return print(text);
    } else {
        return print_delay(text);
    }
}

function prompt(text, cb = false){
    term.prompt(text);
    if(cb){
        return new Promise((resolve, reject)=>{
            prompt_cb = (answer)=>{
                prompt_cb = null;
                resolve(answer);
            }
        });
    }
}

function set_mask(mask){
    term.mask(mask ? '' : false);
    if(mask) term.history().disable();
    else term.history().enable();
}

function set_echo(e){
    echo = e;
}

async function LogReg(){
    var answer = await prompt('login/register? ', true);
    switch(answer.toLowerCase().trim()[0]){
        case 'l':
            Login();
        break;
        case 'r':
            Register();
        break;
        default:
            LogReg();
        break;
    }
}

async function Login(){
    var username = await prompt('login: ', true);
    set_mask(true);
    var password = await prompt('password: ', true);
    socket.emit('data', {request: "auth", login: {username: username, password: password}});
}

async function Register(){
    var username = await prompt('username: ', true);
    set_mask(true);
    var password = await prompt('password: ', true);
    socket.emit('data', {request: "auth", register: {username: username, password: password}});
}

var term;
pre.html('[Intercept Web Client]\n');

window.eventHandler = (dobj)=>{
    switch(dobj.event){
        case 'info':
            if(!token){
                mask = false;
                echo = false;
                LogReg();
            }
        break;
        case 'auth':
            if(dobj.success){
                var first = false;
                if(!cfg) first = true;
                cfg = dobj.cfg;
                setVolume();
                if(first) playSound(music_peaceful);
                socket.emit('data', {request: 'connect', token: dobj.token});
                token = dobj.token;
                set_mask(false);
                set_echo(true);
                prompt('>>');
            } else {
                set_mask(false);
                write(html.encode(dobj.error));
                LogReg();
            }
        break;
        case 'traceStart':
            panicSound(true);
            write(html.encode(dobj.msg));
        break;
        case 'traceComplete':
            panicSound(false);
            if(dobj.msg) write(html.encode(dobj.msg));
        break;
        case 'cfg':
            var first = false;
            if(!cfg) first = true;
            cfg = dobj.cfg;
            setVolume();
            if(first) playSound(music_peaceful);
        break;
        case 'connect':
            if(dobj.msg){
                write(html.encode(dobj.msg));
            }
        break;
        case 'chat':
            write(html.encode(dobj.msg));
        break;
        case 'command':
            if(dobj.msg != '')
                write(html.encode(dobj.msg.replace(/\u200b/g, ' ').replace(/\t/g, '  ')));
        break;
        case 'error':
            write(html.encode(dobj.error));
        break;
        case 'broadcast':
            write(html.encode(dobj.msg));
        break;
    }
};

window.terminalWrite = function(text){
    write(text);
}

function echo_t(cmd, command){
    if(!echo) return;
    write(`${term.prompt()}${command}`);
}

if(window.localStorage.getItem('intercept_macros') == undefined){
    window.localStorage.setItem('intercept_macros', JSON.stringify({}));
}

var macros = JSON.parse(window.localStorage.getItem('intercept_macros'));

function macroReplace(txt){
    Object.keys(macros).forEach((name)=>{
        var reg = new RegExp(`\\$${name}`, 'g');
        txt = txt.replace(reg, macros[name]);
    });
    return txt;
}

term = $('#input').cmd({
    prompt: '>> ',
    width: '100%',
    commands: function(command){
        if(command.length == 0){
            delays.forEach(s=>{
                s.val = s.raw.length;
            });
            return;
        }
        echo_t(this, command);
        if(command == 'clear') {
            pre.html('[Intercept Web Client]\n');
        } else if(command == 'delay'){
            if(write_method == 'print'){
                write_method = 'print_delay';
                write('Write method changed to ¬Pprint_delay¬*.');
            } else {
                write_method = 'print';
                write('Write method changed to ¬Pprint¬*.');
            }
        } else if(command == "test_con"){
            socket.close();
        } else {
            command = macroReplace(command);
            if(command[0] == '!'){
                if(command.length == 1){
                    write(`[--Macro List--]\n${Object.keys(macros).map(el=>{
                        return `¬g$${el} ¬P=> ¬b${macros[el]}`;
                    }).join('\n')}`);
                } else {
                    command = command.substring(1);
                    var name = command.split(' ')[0];
                    var val = command.split(' ').slice(1).join(' ');
                    if(val.length > 0){
                        macros[name] = val;
                        window.localStorage.setItem('intercept_macros', JSON.stringify(macros));
                        write(`Macro $${name} set to: ${val}`);
                    } else {
                        delete macros[name];
                        window.localStorage.setItem('intercept_macros', JSON.stringify(macros));
                        write(`Macro $${name} deleted.`);
                    }
                }
            } else {
                if(prompt_cb != null){
                    prompt_cb(command);
                } else {
                    socket.emit('data', {request: "command", cmd: command});
                }
            }
        }
    }
});

term.history().clear();

init();