const io = require('socket.io-client');

module.exports = function(window){
    var socket = io('http://209.97.136.54:13370');

    socket.on('error', (err)=>{
        window.send('print', err.message);
    });

    socket.on('data', (data)=>{
        window.send('event', data);
    });

    socket.on('connect_timeout', (err)=>{
        window.send('print', 'Connection timed out.');
    });

    socket.on('disconnect', ()=>{
        window.send('print', '¬RDisconnected');
    });

    socket.on('reconnect', ()=>{
        window.send('print', 'Reconnected.');
        socket.emit('data', {request: 'connect', token: token});
    });

    socket.on('reconnect_attempt', (i)=>{
        window.send('print', `(${i}) ¬yReconnecting...`);
    });

    return socket;
}