const fs = require('fs');
switch(process.platform){
    case 'win32':
        fs.copyFileSync('manifests/windows.toml', 'dist/win-unpacked/.itch.toml');
    break;
    case 'linux':
        fs.copyFileSync('manifests/linux.toml', 'dist/linux-unpacked/.itch.toml');
    break;
    default:
        throw new Error('Platform unsupported.');
    break;
}