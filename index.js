const electron = require('electron');
const Path = require('path');
let twin;

let chat_windows = new Map();

var ipc = electron.ipcMain;

function loadTestWindow(){
    twin = new electron.BrowserWindow({show: false, width: 800, height: 600, webPreferences:{
        devTools: true,
        nodeIntegration: false,
        sandbox: true,
        preload: Path.join(__dirname, 'preload.js')
    }});
    ipc.once('init', ()=>{
        let socket = require('./ws')(twin.webContents);
        ipc.on('request', (event, d)=>{
            socket.emit('data', d);
        });
    });
    twin.loadFile(`Source/index.html`);
    twin.on('ready-to-show', ()=>{
        twin.show();
        twin.maximize();
    });
    twin.on('closed', ()=>{
        twin = null;
        chat_windows.forEach(chat=>{
            if(chat)
                chat.close();
        });
    });
}

electron.app.on('ready', loadTestWindow);

electron.app.on('window-all-closed', () => {
    if(process.platform !== 'darwin'){
        electron.app.quit();
    }
});