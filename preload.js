try {
    const {ipcRenderer} = require('electron');

    window.eventHandler = null;

    window.terminalWrite = null;

    window.init = function(){
        ipcRenderer.send('init');
    }

    window.socket = {
        emit: function(event, obj){
            ipcRenderer.send('request', obj);
        }
    }

    ipcRenderer.on('event', (event, d)=>{
        if(window.eventHandler != null) {
            window.eventHandler(d);
        }
    });

    ipcRenderer.on('print', (event, d)=>{
        if(window.terminalWrite != null) window.terminalWrite(d);
    });

    console.log('Preload complete');
} catch(e) {
    console.error(e);
}